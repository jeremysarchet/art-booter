# Art Booter

With shell scripting, setup a Linux computer to play a video in loop upon
booting, and stop playing upon shutdown.

**Note: This codebase is still experimental and not yet fully implemented.**

(Feel free to contact me if you'd like assistance getting it going with your own
setup.)

## Credits:

Primarily derived from Steven Hickson's codebase: https://github.com/StevenHickson/RPiVideoLooper

## Context

I aspire to produce my own physical video packages.

I'd like to integrate a display and a mini single-board computer (e.g.
Raspberry Pi) into a unified package with a single power switch.

The package is a single-purpose device that when you power it on, it starts
playing a given video, seamlessly in loop, and when you're done you power it
off.

There's a whole slew of "digital canvases" on the market, and as far as I can
tell, the business model is all about subscription, like spotify, and the
displays themselves are all "smart" devices. I find it appealing to be able
to offer a "dumb" device.

My vision is to be able to offer video artworks along the same lines as print
artworks, that is, physical things that you hang on your wall (albeit, these
ones will need to be plugged in).

## Usage

Copy the codebase to the Pi and execute `./install.sh` to install.

Installation copies the script `videoloop` to `/etc/init.d` and sets it to run
at boot.

`videoloop` looks for a video file of extension `.h264` in `/mnt/deanimation`.

Currently, the idea is to have the video located on a thumb drive, and
auto-mounted at boot via `fstab`. To set this up, I've added the line:

```
UUID=<uuid> /mnt/deanimation <filesystem> defaults,nofail,noatime 0 0
```

to `/etc/fstab`.


## Setting up Hello Video

From: https://github.com/adafruit/pi_hello_video

In the directory `/opt/vc/src/hello_pi/`.

First build it by executing `./rebuild.sh`.

Install by executing `sudo make install` inside `hello_video` directory.

Run by executing `hello_video.bin`. With no parameters the usage will be printed.

### Tweak Hello Video to play in seamless loop:

From: http://pi.bek.no/loopVideo/

To make `hello_video` play in a seamless loop, you need to change its source
code. Edit:

`/opt/vc/src/hello_pi/hello_video/video.c`

Replacing:

```C
if(!data_len)
    break;
```
with:

```C
if(!data_len)  
    fseek(in, 0, SEEK_SET);
```

Ctrl-x to exit and save. Compile video.c with `make`.

Now when you run the `./hello_video.bin my-vid.h264` command, the video will
play in loop.


### Convert to raw h.264 format

Here's a simple invocation to convert a video to raw h.264 using ffmpeg:

`ffmpeg -i in.mp4 -vcodec copy -an -bsf:v h264_mp4toannexb out.h264`

## Extras

### Concatenate a folder of videos

In the case of wanting to combine a collection of videos into one single video,
I like the tool `ffmpeg-concat` which provides a simple interface for
concatenation with a variety of different transitions.

https://github.com/transitive-bullshit/ffmpeg-concat

For example: concatenate all .mp4 files in the current directory with 2
second (2000 milisecond) crossfades

`ffmpeg-concat -d 2000 *.mp4`

If you want the final concatenation product to itself be a seamless loop with a
crossfade, a simple approach is to slice the first video into two parts and
then feed the tail into the start of the concatenation and the head at the end.

Slice a given video at the two second mark:

`ffmpeg -t 2 -i in.mp4 head.mp4`

`ffmpeg -ss 2 -i in.mp4 tail.mp4`

## Setting up a USB key to mount at boot with `fstab`

Entry added to `/etc/fstab`
```
UUID=ABCD-1234 /mnt/deanimation vfat defaults,nofail,noatime 0 0
```

Configuring external storage devices  
https://www.raspberrypi.org/documentation/configuration/external-storage.md

## Other related references

Steve Parker's Shell Scripting Tutorial  
https://www.shellscript.sh/

Raspberry Pi - run program at start-up  
https://www.stuffaboutcode.com/2012/06/raspberry-pi-run-program-at-start-up.html

hello_video  
https://github.com/raspberrypi/firmware/tree/master/opt/vc/src/hello_pi/hello_video  
https://github.com/raspberrypi/userland/tree/master/host_applications/linux/apps/hello_pi/hello_video
https://learn.adafruit.com/raspberry-pi-video-looper/hello-video  
https://github.com/adafruit/pi_video_looper  

Play video  
http://pi.bek.no/playVideo/

Auto start program on boot  
http://pi.bek.no/autostartProgramOnBoot/

Raspberry Pi Automatic Video Looper – Steves Computer Vision Blog  
http://stevenhickson.blogspot.com/2013/09/raspberry-pi-automatic-video-looper.html  

Seamless Looping  
https://www.raspberrypi.org/forums/viewtopic.php?f=38&t=8042

A variety of ways to auto-run programs on boot  
https://www.dexterindustries.com/howto/auto-run-python-programs-on-the-raspberry-pi/
